﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Core;
using Assets.Scripts.Player;
using Assets.Scripts.Presentation;
using Assets.Scripts.UI;
using Photon.Pun;
using UnityEngine;

namespace Assets.Scripts.Directors
{
    public class RoundDirector : MonoBehaviour
    {
        public PlayerInputBar[] PlayerInputBars;
        public RoundSequencer RoundSequencer;
        public FightMenu FightMenu;

        PlayingCharacter[] players;
        private int[] playerInputCount;
        private Dictionary<int, List<FightActionType>> fightActions;

        private bool LastPossibleInput => 
            fightActions.All(kvp => kvp.Value.Count >= 5);
        
        public void Initialize(PlayingCharacter player1, PlayingCharacter player2)
        {
            players = new[] {player1, player2};
            Reset();
        }

        void Reset()
        {
            playerInputCount = new[] {0, 0};
            fightActions = new Dictionary<int, List<FightActionType>>();
            fightActions.Add(0, new List<FightActionType>());
            fightActions.Add(1, new List<FightActionType>());

            foreach (var pi in players.Select(p => p.PlayerInput))
                pi.Enable();
        }

        public void QueFightAction(int playerId, FightActionType actionType)
        {
            fightActions[playerId].Add(actionType);
            PlayerInputBars[playerId].QueFightAction();
            playerInputCount[playerId]++;

            if (fightActions[playerId].Count >= 5)
                players[playerId].PlayerInput.Disable();

            if (LastPossibleInput)
                FinishedRoundInput();
        }

        public void FinishedRoundInput() => 
            RoundSequencer.StartRoundSequence(fightActions[0], fightActions[1]);

        public void RoundFinished()
        {
            foreach (var pib in PlayerInputBars)
                pib.Clear();

            Reset();
        }

        public void FightFinished(PlayingCharacter winner)
        {
            bool player1Won = winner.PlayerId == 0;
            bool localPlayerWon;
            if (PhotonNetwork.IsConnected)
            {
                localPlayerWon = PhotonNetwork.IsMasterClient && player1Won || 
                                 !PhotonNetwork.IsMasterClient && !player1Won;
            }
            else
            {
                if (!Session.CpuPlayerEnabled)
                    localPlayerWon = true;
                else
                    localPlayerWon = player1Won;
            }

            FightMenu.ShowAsFightEnded(localPlayerWon);
        }
    }
}
