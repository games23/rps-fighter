﻿using UnityEngine;

namespace Assets.Scripts.UI
{
    public class FightUIController : MonoBehaviour
    {
        public TutorialUI Tutorial;
        public FightMenu Menu;

        void Update()
        {
            if (Input.GetButtonDown("Help"))
                Tutorial.SwitchHelp();

            if (Input.GetButtonDown("Menu"))
                Menu.Switch();
        }
    }
}
