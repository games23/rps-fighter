﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class PlayerInputBar : MonoBehaviour
    {
        public GameObject ActionMarkerPrefab;
        public Transform InputQueContainer;

        private readonly List<GameObject> markers = new List<GameObject>();

        public void QueFightAction()
        {
            var marker = Instantiate(ActionMarkerPrefab, InputQueContainer);
            markers.Add(marker);
        }

        public void Clear()
        {
            foreach (var m in markers)
                Destroy(m);

            markers.Clear();
        }
    }
}
