﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class TutorialUI : MonoBehaviour
    {
        public GameObject HelpPanel;

        public void SwitchHelp() =>
            HelpPanel.SetActive(!HelpPanel.activeInHierarchy);
    }
}
