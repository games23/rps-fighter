﻿using Assets.Scripts.Core;
using Assets.Scripts.Directors;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class PlayerInput : MonoBehaviour
    {
        public int PlayerId;
        protected RoundDirector roundDirector;

        public void Initialize(RoundDirector roundDirector) =>
            this.roundDirector = roundDirector;

        public void Disable() => enabled = false;

        public virtual void Enable() => enabled = true;

        protected void QueFightAction(FightActionType actionType) =>
            roundDirector.QueFightAction(PlayerId, actionType);
    }
}