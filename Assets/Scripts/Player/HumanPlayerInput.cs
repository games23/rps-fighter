﻿using Assets.Scripts.Core;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class HumanPlayerInput : PlayerInput
    {
        void Update() => ReadInput();

        private void ReadInput()
        {
            if (Input.GetButtonDown($"Punch{PlayerId}"))
                QueFightAction(FightActionType.Punch);
            if (Input.GetButtonDown($"Kick{PlayerId}"))
                QueFightAction(FightActionType.Kick);
            if (Input.GetButtonDown($"Headbutt{PlayerId}"))
                QueFightAction(FightActionType.Headbutt);
            if (Input.GetButtonDown($"Amplify{PlayerId}"))
                QueFightAction(FightActionType.Amplify);
            if (Input.GetButtonDown($"Block{PlayerId}"))
                QueFightAction(FightActionType.Block);
        }
    }
}
