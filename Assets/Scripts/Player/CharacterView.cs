﻿using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class CharacterView : MonoBehaviour
    {
        private Animator animator;
        private HpBar hpBar;
        private int startingHp;

        public void Initialize(int startingHp, HpBar hpBar, Animator animator)
        {
            this.animator = animator;
            this.startingHp = startingHp;
            this.hpBar = hpBar;
        }

        public void UpdateHp(int currentHp) => 
            hpBar.UpdateValue((float) currentHp / startingHp);

        public void PlayAnimation(string animation)
        {
            Debug.Log(animation);
            animator.SetTrigger(animation);
        }

        public void StopInPlace() => 
            animator.applyRootMotion = true;

        public void Resume() => 
            animator.applyRootMotion = false;
    }
}
