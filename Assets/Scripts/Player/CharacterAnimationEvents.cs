﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Audio;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class CharacterAnimationEvents : MonoBehaviour
    {
        public ParticleSystem AmplifyVfx;
        public ParticleSystem AmplifiedVfx;
        public ParticleSystem Hit1Vfx;

        public CharacterAudio CharacterAudio;

        public void PlayAmplifyVfx() => AmplifyVfx.Play();
        public void PlayAmplifiedVfx() => AmplifiedVfx.Play();
        public void StopAmplifiedVfx() => AmplifiedVfx.Stop();

        public void PlayWooshSfx() => CharacterAudio.PlayClip(ClipType.Woosh);
        public void PlayBlockSfx() => CharacterAudio.PlayClip(ClipType.Block);
        public void PlayAmplifySfx() => CharacterAudio.PlayClip(ClipType.Amplify);
        public void PlayVictorySfx() => CharacterAudio.PlayClip(ClipType.Victory);
        public void PlayKnockOutSfx() => CharacterAudio.PlayClip(ClipType.KnockOut);
        public void PlayThuxSfx() => CharacterAudio.PlayClip(ClipType.Thud);
        public void PlayHurtSfx() => CharacterAudio.PlayClip(ClipType.Grunt);
        public void PlayAttackVoiceSfx() => CharacterAudio.PlayClip(ClipType.Attack);

        public void PlayHitFx()
        {
            Hit1Vfx.Play();
            CharacterAudio.PlayClip(ClipType.Punch);
        }
    }
}
