﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Common;
using UnityEngine;

namespace Assets.Scripts.Audio
{
    public class AudioLibrary : MonoBehaviour
    {
        public ClipLibrary[] Libraries;

        private Dictionary<ClipType, IEnumerable<AudioClip>> libraries;

        public AudioClip GetRandomClip(ClipType clipType) => 
            libraries[clipType].PickOne();
        
        void Start()
        {
            libraries = new Dictionary<ClipType, IEnumerable<AudioClip>>();
            foreach (var l in Libraries)
                libraries.Add(l.ClipType, l.Clips);
        }
    }

    [Serializable]
    public class ClipLibrary
    {
        public ClipType ClipType;
        public AudioClip[] Clips;
    }

    public enum ClipType
    {
        Punch,
        Woosh,
        Grunt,
        Attack,
        KnockOut,
        Victory,
        Amplify,
        Block,
        Thud
    }
}
