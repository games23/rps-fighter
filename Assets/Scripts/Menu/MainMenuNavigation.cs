﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Menu
{
    public class MainMenuNavigation : MonoBehaviour
    {
        public MultiplayerMenu MultiplayerMenu;

        public void StartSoloFight()
        {
            Session.EnableCpuPlayer();
            SceneManager.LoadScene("LocalFight");
        }

        public void StartLocalFight()
        {
            Session.DisableCpuPlayer();
            SceneManager.LoadScene("LocalFight");
        }

        public void GoToMultiplayerOnline() => MultiplayerMenu.Show(gameObject);

        public void ExitGame() => Application.Quit();
    }
}
