﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Core
{
    public class FightAction
    {
        public FightAction(FightActionType actionType, bool isAmplified)
        {
            IsAmplified = isAmplified;
            ActionType = actionType;
            weakerAction = GetWeakerAttack(actionType);
        }

        public bool IsAmplified { get; private set; }
        public FightActionType ActionType { get; private set; }

        static FightActionType[] attacks = { FightActionType.Punch, FightActionType.Kick, FightActionType.Headbutt };

        private FightActionType weakerAction;

        public bool IsAnAttack => attacks.Contains(ActionType);

        public static FightAction CreateAmplifiedFromAttack(FightActionType attackType) => 
            new FightAction(attackType, true);

        public static FightAction CreateFromAction(FightActionType actionType) => 
            new FightAction(actionType, false);

        public bool AttacksAreEqual(FightAction other) =>
            ActionType == other.ActionType &&
            IsAmplified == other.IsAmplified;

        public bool Beats(FightAction other)
        {
            if(IsAnAttack && !other.IsAnAttack)
                return true;

            if (!IsAnAttack && other.IsAnAttack)
                return false;

            if (IsAmplified && !other.IsAmplified)
                return true;
            
            if (!IsAmplified && other.IsAmplified)
                return false;

            return other.ActionType == weakerAction;
        }

        FightActionType GetWeakerAttack(FightActionType actionType)
        {
            if (actionType == FightActionType.Punch)
                return FightActionType.Kick;
            if (actionType == FightActionType.Kick)
                return FightActionType.Headbutt;
            if (actionType == FightActionType.Headbutt)
                return FightActionType.Punch;

            return FightActionType.None;
        }
    }
}