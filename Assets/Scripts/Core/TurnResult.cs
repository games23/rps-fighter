﻿namespace Assets.Scripts.Core
{
    public struct TurnResult
    {
        public bool Attack1Wins { get; private set; }
        public bool Attack2Wins { get; private set; }
        public bool IsATie { get; private set; }
        public bool Attack1Blocked { get; private set; }
        public bool Attack2Blocked { get; private set; }

        public static TurnResult CreateAction1Win() => 
            new TurnResult() {Attack1Wins = true};

        public static TurnResult CreateAction2Win() =>
            new TurnResult() { Attack2Wins = true };

        public static TurnResult CreateTie() =>
            new TurnResult() { IsATie = true };

        public static TurnResult CreateAttack1Blocked() =>
            new TurnResult() { Attack1Blocked = true };
        public static TurnResult CreateAttack2Blocked() =>
            new TurnResult() { Attack2Blocked = true };
    }
}