﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Common
{
    public class DataManager
    {
        private const string PlayerNameKey = "PlayerName";

        public static void SetPlayerName(string name)
        {
            PlayerPrefs.SetString(PlayerNameKey, name);
            PlayerPrefs.Save();
        }

        public static string GetPlayerName() =>
            PlayerPrefs.HasKey(PlayerNameKey) ? 
                PlayerPrefs.GetString(PlayerNameKey) : 
                "PLAYER";


    }
}
