﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Core;
using Assets.Scripts.Directors;
using Assets.Scripts.Player;
using UnityEngine;

namespace Assets.Scripts.Presentation
{
    public class RoundSequencer : MonoBehaviour
    {
        public PlayingCharacter Player1;
        public PlayingCharacter Player2;
        public RoundDirector Director;
        public CameraShake CameraShake;

        public float TurnDuration;

        private FightActionType[] player1Actions;
        private FightActionType[] player2Actions;
        private int turnIndex;

        public void StartRoundSequence(IEnumerable<FightActionType> player1Actions, IEnumerable<FightActionType> player2Actions)
        {
            this.player1Actions = player1Actions.ToArray();
            this.player2Actions = player2Actions.ToArray();
            turnIndex = 0;

            PrepareTurn();
        }

        void PrepareTurn()
        {
            if (turnIndex < 5)
            {
                var action1 = new FightAction(player1Actions[turnIndex], Player1.IsAmplified);
                var action2 = new FightAction(player2Actions[turnIndex], Player2.IsAmplified);

                PlayTurn(action1, action2);

                if (Player1.IsKnockedOut || Player2.IsKnockedOut)
                    StartCoroutine(WaitAndDo(TurnDuration, FinishFight));
                else
                    StartCoroutine(WaitAndDo(TurnDuration, PrepareTurn));

                turnIndex++;
            }
            else
                Director.RoundFinished();

        }

        private void FinishFight()
        {
            var winner = Player1.IsKnockedOut ? Player2 : Player1;
            Director.FightFinished(winner);
            winner.CharacterView.PlayAnimation("Win");
        }

        void PlayTurn(FightAction action1, FightAction action2)
        {
            Player1.ActiveActionIcon.ShowAction(player1Actions[turnIndex]);
            Player2.ActiveActionIcon.ShowAction(player2Actions[turnIndex]);

            var turn = new Turn(Player1, Player2);
            switch (turn.Resolve(action1, action2, CameraShake.Shake))
            {
                case TurnResultEffect.Attack1Blocked:
                    if (turnIndex < 4)
                        player1Actions[turnIndex + 1] = FightActionType.None;
                    break;
                case TurnResultEffect.Attack2Blocked:
                    if (turnIndex < 4)
                        player2Actions[turnIndex + 1] = FightActionType.None;
                    break;
                case TurnResultEffect.Player1KnockedOut:
                case TurnResultEffect.Player2KnockedOut:
                    StopAllCoroutines();
                    break;
            }
        }

        IEnumerator WaitAndDo(float time, Action onTimeCompleted)
        {
            yield return new WaitForSeconds(time);
            onTimeCompleted();
        }
    }
}
