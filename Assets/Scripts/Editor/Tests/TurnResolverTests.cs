﻿using Assets.Scripts.Common;
using Assets.Scripts.Core;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class TurnResolverTests
    {
        [Test]
        public void PunchBeatsKick()
        {
            //given
            var fightAction1 = FightAction.CreateFromAction(FightActionType.Punch);
            var fightAction2 = FightAction.CreateFromAction(FightActionType.Kick);
            //when
            var turnResult = TurnResolver.Resolve(fightAction1, fightAction2);
            //then
            Assert.IsTrue(turnResult.Attack1Wins);
        }

        [Test]
        public void KickLosesAgainstPunch()
        {
            //given
            var fightAction1 = FightAction.CreateFromAction(FightActionType.Kick);
            var fightAction2 = FightAction.CreateFromAction(FightActionType.Punch);
            //when
            var turnResult = TurnResolver.Resolve(fightAction1, fightAction2);
            //then
            Assert.IsTrue(turnResult.Attack2Wins);
        }

        [Test]
        public void KickBeatsHeadbutt()
        {
            //given
            var fightAction1 = FightAction.CreateFromAction(FightActionType.Kick);
            var fightAction2 = FightAction.CreateFromAction(FightActionType.Headbutt);
            //when
            var turnResult = TurnResolver.Resolve(fightAction1, fightAction2);
            //then
            Assert.IsTrue(turnResult.Attack1Wins);
        }

        [Test]
        public void HeadbuttBeatsPunch()
        {
            //given
            var fightAction1 = FightAction.CreateFromAction(FightActionType.Headbutt);
            var fightAction2 = FightAction.CreateFromAction(FightActionType.Punch);
            //when
            var turnResult = TurnResolver.Resolve(fightAction1, fightAction2);
            //then
            Assert.IsTrue(turnResult.Attack1Wins);
        }

        [Test]
        public void AnyKickeatsCharge()
        {
            //given
            var attacks = new[] {FightActionType.Punch, FightActionType.Kick, FightActionType.Headbutt};
            var fightAction1 = FightAction.CreateFromAction(attacks.PickOne());
            var fightAction2 = FightAction.CreateFromAction(FightActionType.Amplify);
            //when
            var turnResult = TurnResolver.Resolve(fightAction1, fightAction2);
            //then
            Assert.IsTrue(turnResult.Attack1Wins);
        }

        [Test]
        public void SameAttackTies()
        {
            //given
            var fightAction1 = FightAction.CreateFromAction(FightActionType.Punch);
            var fightAction2 = FightAction.CreateFromAction(FightActionType.Punch);
            //when
            var turnResult = TurnResolver.Resolve(fightAction1, fightAction2);
            //then
            Assert.IsTrue(turnResult.IsATie);
        }

        [Test]
        public void ChargeAndNoneShouldTie()
        {
            //given
            var fightAction1 = FightAction.CreateFromAction(FightActionType.Amplify);
            var fightAction2 = FightAction.CreateFromAction(FightActionType.None);
            //when
            var turnResult = TurnResolver.Resolve(fightAction1, fightAction2);
            //then
            Assert.IsTrue(turnResult.IsATie);
        }

        [Test]
        public void Attack1ShouldBeBlocked()
        {
            //given
            var fightAction1 = FightAction.CreateFromAction(FightActionType.Punch);
            var fightAction2 = FightAction.CreateFromAction(FightActionType.Block);
            //when
            var turnResult = TurnResolver.Resolve(fightAction1, fightAction2);
            //then
            Assert.IsTrue(turnResult.Attack1Blocked);
        }

        [Test]
        public void Attack2ShouldBeBlocked()
        {
            //given
            var fightAction1 = FightAction.CreateFromAction(FightActionType.Block);
            var fightAction2 = FightAction.CreateFromAction(FightActionType.Punch);
            //when
            var turnResult = TurnResolver.Resolve(fightAction1, fightAction2);
            //then
            Assert.IsTrue(turnResult.Attack2Blocked);
        }

        [Test]
        public void AmplifiedPunchBeatsStandardHeadbutt()
        {
            //given
            var fightAction1 = FightAction.CreateAmplifiedFromAttack(FightActionType.Punch);
            var fightAction2 = FightAction.CreateFromAction(FightActionType.Headbutt);
            //when
            var turnResult = TurnResolver.Resolve(fightAction1, fightAction2);
            //then
            Assert.IsTrue(turnResult.Attack1Wins);
        }

        [Test]
        public void AmplifiedPunchBeatsStandardHeadbutt2()
        {
            //given
            var fightAction1 = FightAction.CreateFromAction(FightActionType.Headbutt);
            var fightAction2 = FightAction.CreateAmplifiedFromAttack(FightActionType.Punch);
            //when
            var turnResult = TurnResolver.Resolve(fightAction1, fightAction2);
            //then
            Assert.IsTrue(turnResult.Attack2Wins);
        }

        [Test]
        public void AmplifiedPunchBeatsAmplifiedKick()
        {
            //given
            var fightAction1 = FightAction.CreateAmplifiedFromAttack(FightActionType.Punch);
            var fightAction2 = FightAction.CreateAmplifiedFromAttack(FightActionType.Kick);
            //when
            var turnResult = TurnResolver.Resolve(fightAction1, fightAction2);
            //then
            Assert.IsTrue(turnResult.Attack1Wins);
        }

        [Test]
        public void AmplifyIsBeatenByPunch()
        {
            //given
            var fightAction1 = FightAction.CreateFromAction(FightActionType.Amplify);
            var fightAction2 = FightAction.CreateFromAction(FightActionType.Punch);
            //when
            var turnResult = TurnResolver.Resolve(fightAction1, fightAction2);
            //then
            Assert.IsTrue(turnResult.Attack2Wins);
        }
    }
}
